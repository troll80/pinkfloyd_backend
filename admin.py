import os
from uuid import uuid4
import logging

from flask import request, redirect, url_for
from flask_admin.contrib.sqla import ModelView
import flask_login as login
from flask_admin import AdminIndexView, helpers, expose, form
from jinja2 import Markup
from wtforms.validators import DataRequired

from form import LoginForm

file_path = os.path.join(os.path.dirname(__file__), 'tmp')

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
# create formatter
formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
# add formatter to ch
ch.setFormatter(formatter)

# add ch to logger
logger.addHandler(ch)

def prefix_name(obj, file_name):
    name = str(uuid4()).split('-')[0]
    name_ext = ('.').join([name, 'jpg'])
    return name_ext


class BaseView(ModelView):

    def is_accessible(self):
       return login.current_user.is_authenticated       
       
    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for('login', next=request.url))

    create_template = 'edit.html'
    edit_template = 'edit.html'


class ImageView(BaseView):
    
    def _list_thumbnail(view, context, model, name):
        if not model.path:
            return ''

        return Markup('<img src="%s"' % url_for('static', filename=form.thumbgen_filename(model.path)))


    """
    def on_model_change(self, *args, **kwargs):
        pass
        
    def validate_form(self, form):
        pass
     """    
    column_formatters = {
        'path': _list_thumbnail
    }

    form_extra_fields = {
        'path': form.ImageUploadField('Image',
                                       namegen=prefix_name,
                                       base_path=file_path, 
                                       thumbnail_size=(200, 150, True))}


class EventView(ModelView):
    pass


class UserAdminIndexView(AdminIndexView):

    @expose('/')
    def index(self):
        if not login.current_user.is_authenticated:
            print(login)
            return redirect(url_for('.login_view'))

        return super(UserAdminIndexView, self).index()

    
    @expose('/login/', methods=('GET','POST'))
    def login_view(self):
        logger.info('LOGIN')
        form = LoginForm(request.form)
        logger.debug(form)
        if helpers.validate_form_on_submit(form):
            user = form.get_user()
            login.login_user(user)

        if login.current_user.is_authenticated:
            return redirect(url_for('.index'))
        
        link = '<p>Input login and pasword for admin.</p>'
        self._template_args['form'] = form
        self._template_args['link'] = link
        return super(UserAdminIndexView, self).index()

    @expose('logout')
    def logout_view(self):
        login.logout_user()
        return redirect(url_for('.index'))