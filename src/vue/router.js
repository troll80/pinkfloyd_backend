import Vue from 'vue'
import VueRouter from 'vue-router'

import Index from './components/Index'
import Catalog from './components/catalog'
import Event from './components/event'
import Image from './components/image'
import Contacts from './components/contacts'

Vue.use(VueRouter)

const routes = [
{ path: '/', component: Index, name: 'index' },
{ path: '/catalog/:tag', component: Catalog, name: 'catalog' },
{ path: '/event/:name', component: Event, name: 'event'},
{ path: '/detail_once/:src', component: Image, name: 'Image', props:true},
{ path: '/contacts', component: Contacts, name:'Contacts'}

 ]


export default new VueRouter({
	routes,
	mode: 'history'
}) 