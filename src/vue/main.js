import Vue from 'vue'

import Vuetify from 'vuetify'
//import SocialSharing from 'vue-social-sharing'

// index.js or main.js
import 'vuetify/dist/vuetify.min.css' 

import App from './components/App.vue'
import router from './router.js'
import axios from 'axios'

//import colors from 'vuetify/es5/util/colors'

Vue.use(Vuetify);
//Vue.use(SocialSharing);

new Vue({
  router,
  el: '#app',
  /*
  theme: {
    primary: colors.red.darken1, // #E53935
    secondary: colors.red.lighten4, // #FFCDD2
    accent: colors.indigo.base // #3F51B5
  },*/
  render: h => h(App)
})
