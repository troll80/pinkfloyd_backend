import logging
import random
from flask import Flask
from flask import render_template, url_for, redirect

from flask_script import Manager, Command
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate,  MigrateCommand
from flask_admin import Admin
from flask_bcrypt import Bcrypt
import flask_login as login
from flask import jsonify


def create_app():

    app = Flask(__name__, template_folder="templates", static_folder="static")
    app.config['SECRET_KEY'] = '123456790'
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
    app.config['DEBUG'] = True

    return app


app = create_app()
bcrypt = Bcrypt(app)
db = SQLAlchemy(app)

from admin import UserAdminIndexView

admin = Admin(app, name='microblog', base_template='master.html', index_view=UserAdminIndexView())




def init_login():
    login_manager = login.LoginManager()
    login_manager.init_app(app)

    # Create user loader function
    @login_manager.user_loader
    def load_user(user_id):
        return db.session.query(User).get(user_id)


init_login()


@app.errorhandler(404)
def page_error_404():
    return render_template('page_error_404.html'), 404


class DBInit(Command):
    def run(self):
        for name in ['wedding', 'events', 'portraits']:
            category = Category()
            category.name = name
            category.save()

        from models import User
        user = User()
        user.login = "admin"
        user.password = "admin"
        user.save()


from models import Image, User, Event, Category
from admin import ImageView, EventView
from flask_admin.contrib.sqla import ModelView

admin.add_view(ImageView(Image, db.session))
#admin.add_view(ModelView(User, db.session))
#admin.add_view(ModelView(Category, db.session))
admin.add_view(EventView(Event, db.session))
#admin.add_view(ModelView(Image, db.session))


@app.route("/")
def index():
    return render_template("index.html")

@app.route('/api/v1/tag/<tag>')
def tag_view(tag):
    events = Event.query.join(Category).filter(Category.name == tag)    
    result = [dict(name=e.name, src='/static/{}'.format(e.images[0].path), url='/event/{}'.format(e.id)) for e in events]
    print(result)
    return jsonify(*result)


@app.route('/api/v1/event/<name>')
def event_view(name):
    q = Image.query.join(Event).filter(Event.name == name) 
    result = [dict(src=i.path, title='title') for i in q]
    return jsonify(*result)

def calcFlex():
    return [12, 6, 3][random.randint(0, 2)]



@app.errorhandler(404)
def page_not_found(e):
    return redirect(url_for('index'))



migrate = Migrate(app, db)
manager = Manager(app)

manager.add_command('db', MigrateCommand)
manager.add_command('init', DBInit())
#manager.add_command('createuser', CreateUser())
