import os
import uuid
import logging

from sqlalchemy.event import listens_for
from sqlalchemy import Column, Integer, \
    Unicode, Text, \
    DateTime, Boolean, \
    ForeignKey
from sqlalchemy.ext.hybrid import hybrid_property
from manage import db, bcrypt

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
# create formatter
formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
# add formatter to ch
ch.setFormatter(formatter)

# add ch to logger
logger.addHandler(ch)


file_path = os.path.join(os.path.dirname(__file__), 'static', 'images')


class MixinModel:

    def save(self):
        db.session.add(self)
        db.session.commit()


class User(db.Model, MixinModel):
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True)
    login = Column(Unicode(128), nullable=False)
    password = Column(Unicode(512), nullable=False)
    _password_hash = Column(Unicode(128), nullable=False)

    @hybrid_property
    def password(self):
        return self._password_hash

    @password.setter
    def password(self, plaintext):
        self._password_hash = bcrypt.generate_password_hash(plaintext)

    def is_check_password(self, plaintext):
        return bcrypt.check_password_hash(self._password_hash, plaintext)

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return self.id

    def __repr__(self):
        return '<User %r>' % (self.login)


class Image(db.Model, MixinModel):

    __tablename__ = 'image'

    id = db.Column(db.Integer, primary_key=True)
    path = db.Column('path', db.Unicode(256), nullable=False)
    flex = db.Column('flex', db.Enum('12','6','3'), default='6')
    
    event_id = db.Column(db.Integer, db.ForeignKey('event.id'), nullable=False) 

    def __repr__(self):
        return '<{}>'.format(self.id)


class Event(db.Model, MixinModel):

    __tablename__ = 'event'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column('Name', db.Unicode(512), nullable=False)

    images = db.relationship('Image', backref=db.backref('event'), cascade="all, delete-orphan", lazy='dynamic') 

    category_id = db.Column(db.Integer, db.ForeignKey(
        'category.id'), nullable=False) 
  
    def __repr__(self):
        return '<{}>'.format(self.name)


class Category(db.Model, MixinModel):

    __tablename__ = 'category'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column('Category', db.Unicode(512))

    events = db.relationship('Event', backref=db.backref('category'), cascade='all, delete-orphan', lazy='dynamic')

    def __repr__(self):
        return '<{}>'.format(self.name)


@listens_for(Category, 'before_insert')
def create_category_dir(mapper, connection, target):
    try:
        os.mkdir(os.path.join(file_path, target.name))
    except OSError as err:
        logger.error(err)


@listens_for(Event, 'before_insert')
def create_event_dir(mapper, connection, target):
    try:
        os.mkdir(os.path.join(file_path, target.category.name, target.name))
    except OSError as err:
        logger.error(err)


@listens_for(Image, 'before_insert')
def insert_path_image(mapper, connection, target):

    src = os.path.join('tmp', target.path)
    dest = os.path.join(
        file_path, target.event.category.name, target.event.name, target.path)

    # path for thumbnail image
    thumb = '{}_thumb.{}'.format(*target.path.split('.'))
    src_thumb = os.path.join('tmp', thumb)
    dest_thumb = os.path.join(
        file_path, target.event.category.name, target.event.name, thumb)

    try:
        os.rename(src, dest)
        os.rename(src_thumb, dest_thumb)
    except OSError as err:
        logger.error(err)

    try:
        target.path = os.path.join(
            'images', target.event.category.name, target.event.name, target.path)
    except OSError as err:
        logger.error(err)


@listens_for(Image, 'after_delete')
def del_image(mapper, contection, target):
    if target.path:
        thumb = '{}_thumb.{}'.format(*target.path.split('.'))
        dest_thumb = os.path.join(
            file_path, target.event.category.name, target.event.name, thumb)
        try:
            os.remove(os.path.join(file_path, target.path))
        except OSError as err:
            logger.error(err)

        try:
            os.remove(os.path.join(
                file_path, dest_thumb))
        except OSError as err:
            logger.error(err)
